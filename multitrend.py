#!/usr/bin/env python3
# coding: utf-8

from statistics import stdev
import math
import sys

import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_finance import candlestick2_ohlc

# --- EDITABLE VARIABLES

lengthLarge = 1000
lengthSmall = lengthLarge * 4

lookback1 = 50
nbDeviation1 = 1

lookback2 = 100
nbDeviation2 = 2

# --- END

factor = 0
current_position_small = lengthSmall
current_position_large = lengthLarge
leftCounter = 0
rightCounter = 0
slopeSmall = 0
slopeLarge = 0
y1top = 0
y1bottom = 0
y2top = 0
y2bottom = 0

def collect_args ():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--small',type=str,help='OHLC file path for smaller time frame')
    parser.add_argument('-l','--large',type=str,help='OHLC file path for larger time frame')
    parser.add_argument('-f','--factor',type=int,default=4,help='Larger time series as a multiple of Smaller time series')
    args = parser.parse_args()

    return args

def press(event, fig, ax1, ax2, ohlcSmall, ohlcLarge, lookback, lookback2, nbDeviation1, nbDeviation2):
    sys.stdout.flush()
    global factor, lengthSmall, lengthLarge, current_position_small, current_position_large, leftCounter, rightCounter
    if event.key == 'left':
        leftCounter += 1
        rightCounter -= 1
        if (leftCounter == factor):
            ax2.cla()
            leftCounter = 0
            rightCounter = 0
            lengthLarge -= 1
            current_position_large -= 1
            plot_chart_large(ax2, ohlcLarge, lengthLarge,current_position_large,lookback2,nbDeviation2)
        lengthSmall -= 1
        current_position_small -= 1
        ax1.cla()
        plot_chart_small(fig, ax1, ohlcSmall, lengthSmall,current_position_small,lookback2,nbDeviation2)
        plot_chart_small(fig, ax1, ohlcSmall, lengthSmall,current_position_small,lookback,nbDeviation1)
        line_intersection(y1top,y1bottom,y2top,y2bottom)
        fig.canvas.draw()
    elif event.key == 'right':
        rightCounter += 1
        leftCounter -= 1
        if (rightCounter == factor):
            ax2.cla()
            leftCounter = 0
            rightCounter = 0
            lengthLarge += 1
            current_position_large += 1
            slopeLarge = plot_chart_large(ax2, ohlcLarge, lengthLarge,current_position_large,lookback2,nbDeviation2)
        lengthSmall += 1
        current_position_small += 1
        ax1.cla()
        plot_chart_small(fig, ax1, ohlcSmall, lengthSmall,current_position_small,lookback2,nbDeviation2)
        plot_chart_small(fig, ax1, ohlcSmall, lengthSmall,current_position_small,lookback,nbDeviation1)
        line_intersection(y1top,y1bottom,y2top,y2bottom)
        fig.canvas.draw()
    elif event.key == 'a':
        lengthLarge -= 1
        current_position_large -= 1
        ax2.cla() 
        plot_chart_large(ax2, ohlcLarge, lengthLarge,current_position_large,lookback2,nbDeviation2)
        lengthSmall -= 4
        current_position_small -= factor
        ax1.cla()
        plot_chart_small(fig, ax1, ohlcSmall,lengthSmall,current_position_small,lookback2,nbDeviation2)
        plot_chart_small(fig, ax1, ohlcSmall,lengthSmall,current_position_small,lookback,nbDeviation1)
        line_intersection(y1top,y1bottom,y2top,y2bottom)        
        fig.canvas.draw()
    elif event.key == 'd':
        lengthLarge += 1
        current_position_large += 1
        ax2.cla()
        plot_chart_large(ax2, ohlcLarge, lengthLarge,current_position_large,lookback2,nbDeviation2)
        lengthSmall += 4
        current_position_small += factor
        ax1.cla()
        plot_chart_small(fig, ax1, ohlcSmall,lengthSmall,current_position_small,lookback2,nbDeviation2)
        plot_chart_small(fig, ax1, ohlcSmall,lengthSmall,current_position_small,lookback,nbDeviation1)
        line_intersection(y1top,y1bottom,y2top,y2bottom)
        fig.canvas.draw()
    elif event.key == 'e':
        lengthLarge = lookback2
        lengthSmall = lengthLarge * factor
        current_position_large = lengthLarge
        current_position_small = lengthSmall
        ax1.cla()
        ax2.cla()
        ax2.set_xbound([0,lookback2])
        plot_chart_large(ax2, ohlcLarge,lengthLarge,current_position_large,lookback2,nbDeviation2)
        ax1.set_xbound([0,lookback2*factor])
        plot_chart_small(fig, ax1, ohlcSmall,lengthSmall,current_position_small,lookback2,nbDeviation2)
        plot_chart_small(fig, ax1, ohlcSmall,lengthSmall,current_position_small,lookback,nbDeviation1)
        line_intersection(y1top,y1bottom,y2top,y2bottom)
        fig.canvas.draw()

def plot_chart_small(fig, ax1,ohlc,length,current_position,lookback,nbDev):
    
    ohlc = ohlc[:length]
    x_sum = 0
    y_sum = 0
    xy_sum = 0
    x2_sum = 0

    plot_end = current_position
    plot_start = current_position - lookback

    for cmpt in range(plot_start,plot_end):
        x_temp = cmpt
        y_temp = ohlc.close[cmpt]
        x_sum = x_sum + x_temp
        y_sum = y_sum + y_temp
        xy_sum = xy_sum + (x_temp * y_temp)
        x2_sum = x2_sum + (x_temp * x_temp)

    if (x2_sum == x_sum * x_sum):
        b = xy_sum - x_sum * y_sum
    else:
        b = (lookback * xy_sum - x_sum * y_sum) / (lookback * x2_sum - x_sum * x_sum)

    a = (y_sum - b * x_sum) / lookback

    maxDeviation = 0
    sumDeviation = 0

    for dev_iter in range(plot_start,plot_end):
        price = b * dev_iter + a
        maxDeviation = max(abs(ohlc.close[dev_iter] - price), maxDeviation) 
        sumDeviation += pow(ohlc.close[dev_iter] - price, 2)

    stdDeviation = math.sqrt(sumDeviation / lookback) * nbDev

    pr1 = b * plot_start + a
    pr2 = b * plot_end + a

    ohlc = ohlc[:length]
    candlestick2_ohlc(ax1, ohlc.open, ohlc.high, ohlc.low, ohlc.close, width=1, colorup='green', colordown='red')

    ax1.plot([plot_start,plot_end],[pr1,pr2], color='black')
    ax1.plot([plot_start,plot_end],[pr1 + stdDeviation,pr2 + stdDeviation], color='black')
    ax1.plot([plot_start,plot_end],[pr1 - stdDeviation,pr2 - stdDeviation], color='black')

    global slopeSmall, y1top, y1bottom, y2top, y2bottom
    if lookback == lookback2:
        slopeSmall = ((pr2 + stdDeviation) - (pr1 + stdDeviation)) / (plot_end - plot_start)
        y2top = pr2 + stdDeviation
        y2bottom = pr2 - stdDeviation
    else:
        y1top = pr2 + stdDeviation
        y1bottom = pr2 - stdDeviation

def plot_chart_large(ax2, ohlc,length,current_position,lookback,nbDev):
    
    ohlc = ohlc[:length]
    x_sum = 0
    y_sum = 0
    xy_sum = 0
    x2_sum = 0

    plot_end = current_position
    plot_start = current_position - lookback

    for cmpt in range(plot_start,plot_end):
        x_temp = cmpt
        y_temp = ohlc.close[cmpt]
        x_sum = x_sum + x_temp
        y_sum = y_sum + y_temp
        xy_sum = xy_sum + (x_temp * y_temp)
        x2_sum = x2_sum + (x_temp * x_temp)


    if (x2_sum == x_sum * x_sum):
        b = xy_sum - x_sum * y_sum
    else:
        b = (lookback * xy_sum - x_sum * y_sum) / (lookback * x2_sum - x_sum * x_sum)

    a = (y_sum - b * x_sum) / lookback

    maxDeviation = 0
    sumDeviation = 0

    for dev_iter in range(plot_start,plot_end):
        price = b * dev_iter + a
        maxDeviation = max(abs(ohlc.close[dev_iter] - price), maxDeviation) 
        sumDeviation += pow(ohlc.close[dev_iter] - price, 2)

    stdDeviation = math.sqrt(sumDeviation / lookback) * nbDev

    pr1 = b * plot_start + a
    pr2 = b * plot_end + a

    ohlc = ohlc[:length]
    candlestick2_ohlc(ax2, ohlc.open, ohlc.high, ohlc.low, ohlc.close, width=1, colorup='green', colordown='red')

    ax2.plot([plot_start,plot_end],[pr1 + stdDeviation,pr2 + stdDeviation], color='black')
    ax2.plot([plot_start,plot_end],[pr1 - stdDeviation,pr2 - stdDeviation], color='black')

    global slopeLarge
    slopeLarge = ((pr2 + stdDeviation) - (pr1 + stdDeviation)) / (plot_end - plot_start)

def line_intersection(y1top,y1bottom,y2top,y2bottom):
    global slopeSmall, slopeLarge
    if slopeSmall > 0 and slopeLarge > 0:
        if y1bottom < y2bottom:
            print('50 lookback LOWER than 100 lookback')
    elif slopeSmall < 0 and slopeLarge < 0:
        if y1top > y2top:
            print('50 lookback GREATER than 100 lookback')

def main():
    args = collect_args()

    fig, (ax1,ax2)= plt.subplots(2,1, figsize=(40,20))
    
    global factor
    factor = args.factor
    print(factor)

    ohlcSmall = pd.read_csv(args.small)
    ohlcSmall = ohlcSmall[:lengthSmall]

    ohlcLarge = pd.read_csv(args.large)
    ohlcLarge = ohlcLarge[:lengthLarge]

    fig.canvas.mpl_connect('key_press_event', lambda event: press(event, fig, ax1, ax2, ohlcSmall, ohlcLarge, lookback1, lookback2, nbDeviation1, nbDeviation2))

    plt.show()

if __name__ == '__main__':
    main()

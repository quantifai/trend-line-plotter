#!/usr/bin/env python3
"""
    :author: Satwik G
    :brief: Generate alerts on trendline intersection
"""

from influxdb import DataFrameClient

import argparse
import threading
import collections
import datetime
import pandas as pd
import pytz
import time
import math
import smtplib

# GLOBAL VARIABLES
slopeSmall = 0
slopeLarge = 0
y1top = 0
y1bottom = 0
y2top = 0
y2bottom = 0
counterLarge = 0
lastLarge = 0
barAggregation = pd.DataFrame(columns=['close','high','low','open'])
dfBarLarge = pd.DataFrame(columns=['close','high','low','open'])
emailFlag = False

def collect_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bar', nargs=2, type=int, default=[15,60], help='specify the time series (in minutes) eg. -t 15 60 or --time 15 240')
    parser.add_argument('-l','--lookback', nargs=2, type=int, default=[50,100], help='specify lookback periods eg. 50 100')
    parser.add_argument('-s','--security',nargs=1,type=int,help='securityId')
    args = parser.parse_args()
    return args

def get_candlestick_data(args, client):
    global counterLarge, barAggregation, lastLarge, testVar
    querySmall = 'SELECT open,high,low,close FROM bar ORDER BY time DESC LIMIT 100'
    resultSmall = client.query(querySmall)
    dfBarSmall = pd.DataFrame(resultSmall['bar'])
    
    lastSmall = dfBarSmall.shape[0]
    if (counterLarge < 4):
        counterLarge += 1
        temp_close = resultSmall['bar']['close'][lastSmall-1]
        temp_high = resultSmall['bar']['high'][lastSmall-1]
        temp_low = resultSmall['bar']['low'][lastSmall-1]
        temp_open = resultSmall['bar']['open'][lastSmall-1]
        barAggregation.loc[counterLarge] = [temp_close,temp_high,temp_low,temp_open]
    if (counterLarge == 4):
        temp_close = barAggregation.close[4]
        temp_high = max(barAggregation['high'])
        temp_low = min(barAggregation['low'])
        temp_open = barAggregation.open[1]
        dfBarLarge.loc[lastLarge] = [temp_close,temp_high,temp_low,temp_open]
        barAggregation = barAggregation.iloc[0:0]
        counterLarge = 0
        lastLarge += 1

    maxLookBack = 100
    lookback = [50,100]
    if lastLarge >= maxLookBack:
        calculate_trendline_large(args, dfBarLarge, lastLarge, maxLookBack, maxLookBack)
        for lIter in range(0,2):
            calculate_trendline_small(args, dfBarSmall, lastSmall, lookback[lIter], maxLookBack)
    
    detect_intersection()
    threading.Timer(15*60,get_candlestick_data, args=[args,client]).start()

def calculate_trendline_small(args, df_bars, last, lookback, maxLookBack):
    ohlc = df_bars[:last]
    x_sum = 0
    y_sum = 0
    xy_sum = 0
    x2_sum = 0

    plot_end = last
    plot_start = last - lookback

    for cmpt in range(plot_start,plot_end):
        x_temp = cmpt
        y_temp = ohlc.close[cmpt]
        x_sum = x_sum + x_temp
        y_sum = y_sum + y_temp
        xy_sum = xy_sum + (x_temp * y_temp)
        x2_sum = x2_sum + (x_temp * x_temp)


    if (x2_sum == x_sum * x_sum):
        b = xy_sum - x_sum * y_sum
    else:
        b = (lookback * xy_sum - x_sum * y_sum) / (lookback * x2_sum - x_sum * x_sum)

    a = (y_sum - b * x_sum) / lookback

    maxDeviation = 0
    sumDeviation = 0

    for dev_iter in range(plot_start,plot_end):
        price = b * dev_iter + a
        maxDeviation = max(abs(ohlc.close[dev_iter] - price), maxDeviation) 
        sumDeviation += pow(ohlc.close[dev_iter] - price, 2)

    stdDeviation = math.sqrt(sumDeviation / lookback) * 1

    pr1 = b * plot_start + a
    pr2 = b * plot_end + a

    global slopeSmall, y1top, y1bottom, y2top, y2bottom
    if lookback == maxLookBack:
        slopeSmall = ((pr2 + stdDeviation) - (pr1 + stdDeviation)) / (plot_end - plot_start)
        y2top = pr2 + stdDeviation
        y2bottom = pr2 - stdDeviation
    else:
        y1top = pr2 + stdDeviation
        y1bottom = pr2 - stdDeviation    

def calculate_trendline_large(args, df_bars, last, lookback, maxLookBack):
    ohlc = df_bars[:last]
    x_sum = 0
    y_sum = 0
    xy_sum = 0
    x2_sum = 0

    plot_end = last
    plot_start = last - lookback

    for cmpt in range(plot_start,plot_end):
        x_temp = cmpt
        y_temp = ohlc.close[cmpt]
        x_sum = x_sum + x_temp
        y_sum = y_sum + y_temp
        xy_sum = xy_sum + (x_temp * y_temp)
        x2_sum = x2_sum + (x_temp * x_temp)


    if (x2_sum == x_sum * x_sum):
        b = xy_sum - x_sum * y_sum
    else:
        b = (lookback * xy_sum - x_sum * y_sum) / (lookback * x2_sum - x_sum * x_sum)

    a = (y_sum - b * x_sum) / lookback

    maxDeviation = 0
    sumDeviation = 0

    for dev_iter in range(plot_start,plot_end):
        price = b * dev_iter + a
        maxDeviation = max(abs(ohlc.close[dev_iter] - price), maxDeviation) 
        sumDeviation += pow(ohlc.close[dev_iter] - price, 2)

    stdDeviation = math.sqrt(sumDeviation / lookback) * 1

    pr1 = b * plot_start + a
    pr2 = b * plot_end + a

    global slopeLarge
    if lookback == maxLookBack:
        slopeLarge = ((pr2 + stdDeviation) - (pr1 + stdDeviation)) / (plot_end - plot_start)

def detect_intersection():
    global slopeSmall, slopeLarge, y1top,y1bottom, y2top, y2bottom, emailFlag
    if slopeSmall > 0 and slopeLarge > 0:
        if y1bottom < y2bottom:
            if emailFlag == False:
                SUBJECT = 'EURUSD UPTREND'
                TEXT = 'Uptrend in MIN_15 and HOUR_1.\nLower band of Lookback 50 crossed Lower band of Lokback 100.\nSlope(HOUR_1): ' + str(slopeLarge) + '\nSlope(MIN_15): ' + str(slopeSmall) + '\ny-position(50): ' + str(y1bottom) + '\ny-position(100): ' + str(y2bottom) + '.'
                emailMsg = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT)
                email_alert(emailMsg)
                emailFlag = True
        else:
            emailFlag = False
    elif slopeSmall < 0 and slopeLarge < 0:
        if y1top > y2top:
            if emailFlag == False:
                SUBJECT = 'EURUSD UPTREND'
                TEXT = 'Uptrend in MIN_15 and HOUR_1.\nLower band of Lookback 50 crossed Lower band of Lokback 100.\nSlope(HOUR_1): ' + str(slopeLarge) + '\nSlope(MIN_15): ' + str(slopeSmall) + '\ny-position(50): ' + str(y1bottom) + '\ny-position(100): ' + str(y2bottom) + '.'
                emailMsg = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT)
                email_alert(emailMsg)
                emailFlag = True
        else:
            emailFlag = False

def email_alert(emailMsg):
    FROM_EMAIL = "quantifaigroup@gmail.com"
    FROM_PASSWORD = "irqusntfnuoqunrw"
    TO_EMAIL = ["gerard@quantifaigroup.com","mayank@quantifaigroup.com","georgia@quantifaigroup.com","satwik@quantifaigroup.com"]
    with smtplib.SMTP("smtp.gmail.com",587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(FROM_EMAIL,FROM_PASSWORD)
        for recepient in TO_EMAIL:
            smtp.sendmail(FROM_EMAIL,recepient,emailMsg)

def main():
    args = collect_args()

    client = DataFrameClient(host='localhost',port=8086,database='algotrader')
    get_candlestick_data(args, client)

if __name__=='__main__':
    main()
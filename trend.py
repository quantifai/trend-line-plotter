#!/usr/bin/env python3
# coding: utf-8

from statistics import stdev
import math
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_finance import candlestick2_ohlc

# --- EDITABLE VARIABLES

length = 15000

lookback = 50
nbDeviation1 = 1

lookback2 = 100
nbDeviation2 = 2

# --- END

current_position = length

def press(event, lookback, lookback2, nbDeviation1, nbDeviation2):
    sys.stdout.flush()
    global length
    global current_position
    global ohlc
    if event.key == 'left':
        length -= 1
        current_position -= 1
        plt.cla()
        plotChart(ohlc, length,current_position,lookback,nbDeviation1)
        plotChart(ohlc, length,current_position,lookback2,nbDeviation2)
        fig.canvas.draw()
    elif event.key == 'right':
        length += 1
        current_position += 1
        plt.cla()
        plotChart(ohlc, length,current_position,lookback,nbDeviation1)
        plotChart(ohlc, length,current_position,lookback2,nbDeviation2)
        fig.canvas.draw()
    elif event.key == 'a':
        length -= 10
        current_position -= 10
        plt.cla()
        plotChart(ohlc,length,current_position,lookback,nbDeviation1)
        plotChart(ohlc,length,current_position,lookback2,nbDeviation2)
        fig.canvas.draw()
    elif event.key == 'd':
        length += 10
        current_position += 10
        plt.cla()
        plotChart(ohlc,length,current_position,lookback,nbDeviation1)
        plotChart(ohlc,length,current_position,lookback2,nbDeviation2)
        fig.canvas.draw()
    elif event.key == 'e':
        length = max(lookback,lookback2)
        current_position = length
        plt.cla()
        plotChart(ohlc,length,current_position,lookback,nbDeviation1)
        plotChart(ohlc,length,current_position,lookback2,nbDeviation2)
        fig.canvas.draw()

    return

def plotChart(ohlc,length,current_position,lookback,nbDev):
    
    ohlc = ohlc[:length]
    x_sum = 0
    y_sum = 0
    xy_sum = 0
    x2_sum = 0

    plot_end = current_position
    plot_start = current_position - lookback

    for cmpt in range(plot_start,plot_end):
        x_temp = cmpt
        y_temp = ohlc.close[cmpt]
        x_sum = x_sum + x_temp
        y_sum = y_sum + y_temp
        xy_sum = xy_sum + (x_temp * y_temp)
        x2_sum = x2_sum + (x_temp * x_temp)


    if (x2_sum == x_sum * x_sum):
        b = xy_sum - x_sum * y_sum
    else:
        b = (lookback * xy_sum - x_sum * y_sum) / (lookback * x2_sum - x_sum * x_sum)

    a = (y_sum - b * x_sum) / lookback

    maxDeviation = 0
    sumDeviation = 0

    for dev_iter in range(plot_start,plot_end):
        price = b * dev_iter + a
        maxDeviation = max(abs(ohlc.close[dev_iter] - price), maxDeviation) 
        sumDeviation += pow(ohlc.close[dev_iter] - price, 2)

    stdDeviation = math.sqrt(sumDeviation / lookback) * nbDev

    pr1 = b * plot_start + a
    pr2 = b * plot_end + a

    ohlc = ohlc[:length]
    candlestick2_ohlc(ax, ohlc.open, ohlc.high, ohlc.low, ohlc.close, width=1, colorup='green', colordown='red')

    ax.plot([plot_start,plot_end],[pr1,pr2], color='black')
    ax.plot([plot_start,plot_end],[pr1 + stdDeviation,pr2 + stdDeviation], color='black')
    ax.plot([plot_start,plot_end],[pr1 - stdDeviation,pr2 - stdDeviation], color='black')


def main():
    fig.canvas.mpl_connect('key_press_event', lambda event: press(event, lookback, lookback2, nbDeviation1, nbDeviation2))


    plt.tight_layout()
    plt.show()

fig, ax = plt.subplots()
ohlc = pd.read_csv('/home/rkfd/dev/test/EURJPY.csv')
ohlc = ohlc[:length]

if __name__ == '__main__':
    main()
